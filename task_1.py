"""
Написати функцію, яка приймає 2 аргументи – цілі числа. Всередині функції виконується
перевірка типу. Якщо хоча б одне з них не int, то повертається 1, якщо обидва int, то
рахується їхня сума. Якщо сума додатня, повертається 0, якщо від’ємна, то -1.
"""


def special(a: int, b: int) -> int:
    if type(a) is not int or type(b) is not int:
        return 1
    summa = a + b
    if summa < 0:
        return -1
    else:
        return 0


if __name__ == "__main__":
    cases = (
        (1, "dd", 1),
        (True, 2, 1),
        (None, 3.14, 1),
        (1, 2, 0),
        (1, 1, 0),
        (-23, 2, -1),
        (-12, -1, -1),
        (-1, 1, 0)
    )

    for arg1, arg2, result in cases:
        assert special(arg1, arg2) == result, f"ERROR: expected -> special({arg1}, {arg2}) == {result}, but got {special(arg1, arg2)}"
